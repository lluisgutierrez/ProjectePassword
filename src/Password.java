
import java.util.Random;

/**
 *
 * @author Lluis
 */
public class Password {
    
    String password;
    private int longitudMinima;
    
    
    public Password(String password){//Creacio del constructor de la contrasenya amb els seus atributs.
       
        int longitud = password.length();
        
        if(longitud>=this.longitudMinima&&longitud<=this.getlongitudMaxima()){
            this.password = password;
        }else{
            System.out.println("La contrasenya es incorrecte");
        }
    }
    
    
    public boolean isStrong(){//Creacio del metode per mirar el nivell de la contrasenya
        boolean forta=false;
        int majuscula=0;
        int minuscula=0;
        int numero=0;
        
        for(int i=0;i<password.length();i++){
           if(Character.isLowerCase(password.charAt(i))){
               majuscula++;
           }else if(Character.isUpperCase(password.charAt(i))){
               minuscula++;
           }else if(Character.isDigit(password.charAt(i))){
               numero++;
           }
        }
        if(numero>=2 && minuscula>=1 && majuscula>=1){
            forta=true;
        }
        return forta;
    }
    
    public String createPassword(){//Creacio del metode per generar la contrasenya
        char[] caracters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        
        String contrasenya = "";
        while(true){
            for (int i = 0; i < 9; i++) {
                contrasenya = contrasenya + caracters[random.nextInt(caracters.length)];
            }
            if(isStrong()==true){
                break;
            }
        }
        System.out.println("La contrasenya final es: "+contrasenya);
        
        this.password = contrasenya;
        
        return contrasenya;
    }

    private int getlongitudMaxima() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}